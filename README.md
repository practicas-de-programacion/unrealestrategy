IKER RAMIREZ UJAQUE

Controles:
	Click derecho abrir/cerrar menu
    Click izquierdo colocar torre

Risquetos Basicos:

 - [X] Capacitat d’elecció, per exemple diferents torretes.
 - [X] El HUD es crea i s’emplena per BP.
 - [X] Ús d’esdeveniments i dispatchers, tot ha d’estar desacoplat.
 - [X] Enemics o objectius a on atacar i en fer-ho que morin.
 - [X] Ús d’un actor component o Interfície.

Riquetos Extra:
	
 - [ ] Fer una pool d’objectes.
 - [X] Augment progressiu de dificultat.
 - [ ] Mapa de joc generat de manera procedimental.
 - [X] Menú inicial amb diversos nivells.
 - [X] Preview
 - [X] “Waves” personalitzables a on posar quantitat d’enemics i tipus d’enemics a aparèixer.
